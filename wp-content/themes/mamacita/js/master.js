$(function(){
  if (!($.browser.msie && $.browser.version === '6.0')) {
    $("#content-text").jScrollPane({
      showArrows: true
    });
  }
  $(".tabs").tabs(".images > div", {
    effect: "fade",
    fadeOutSpeed: 200,
    rotate: true
  }).slideshow({
    autoplay: true
  });

  // $("#home a img").hover(function() {
  //   $(this).attr("src", "../wp-content/themes/mamacita/images/home-active-trans.png");
  // }, function() {
  //   $(this).attr("src", "../wp-content/themes/mamacita/images/home-trans.png");
  // });
  // console.log(document.URL);
  $("#menu a img").hover(function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/menu-active-trans.png");
  }, function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/menu-trans.png");
  });

  $("#reservations a img").hover(function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/reservations-active-trans.png");
  }, function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/reservations-trans.png");
  });

  $("#press a img").hover(function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/press-active-trans.png");
  }, function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/press-trans.png");
  });

  $("#contact a img").hover(function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/contact-active-trans.png");
  }, function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/contact-trans.png");
  });

  $("#shop a img").hover(function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/shop-active-trans.png");
  }, function() {
    $(this).attr("src", "../wp-content/themes/mamacita/images/shop-trans.png");
  });
});
