<?php
/**
 * The template for displaying home.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mamacita
 */

get_header(); ?>

	<div id="content-text" class="homepage">
		<?php global $post; echo $post->post_content; ?>
	</div>
	<div id="tabs-container" class="clearfix">
  		<div class="images-frame clearfix"></div>
  		<div class="images">
    		<div>
     			<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/img_1981_medium.jpg" />
    		</div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/top100main_medium.gif" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/mamacita2.11.9.015_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/mamacita2.11.9.037_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9466_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9496_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9422_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9376_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9280_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9260_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9338_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9594_medium.jpg" />
		    </div>
  		</div>
  		<div class="tabs">
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
  		</div>
  		<hr class="space"/>
  		<div id="backward-forward" class="clearfix">
    		<a class="backward">&laquo;</a>
    		<a class="forward">&raquo;</a>
  		</div>
	</div>
<?php
get_footer();
