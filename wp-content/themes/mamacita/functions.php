<?php
/**
 * mamacita functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package mamacita
 */

if ( ! function_exists( 'mamacita_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function mamacita_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on mamacita, use a find and replace
	 * to change 'mamacita' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'mamacita', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'mamacita' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'mamacita_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'mamacita_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mamacita_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'mamacita_content_width', 640 );
}
add_action( 'after_setup_theme', 'mamacita_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mamacita_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'mamacita' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mamacita' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mamacita_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mamacita_scripts() {
	wp_enqueue_style( 'mamacita-reset', get_template_directory_uri() . '/css/reset.css' );
	wp_enqueue_style( 'mamacita-text', get_template_directory_uri() . '/css/text.css' );
	wp_enqueue_style( 'mamacita-960', get_template_directory_uri() . '/css/960.css' );
	wp_enqueue_style( 'mamacita-jScrollPane', get_template_directory_uri() . '/css/jScrollPane.css' );
	wp_enqueue_style( 'mamacita-shop', get_template_directory_uri() . '/css/shop.css' );
	wp_enqueue_style( 'mamacita-tabs', get_template_directory_uri() . '/css/tabs.css' );
	wp_enqueue_style( 'mamacita-master', get_template_directory_uri() . '/css/master.css' );

	wp_enqueue_script( 'mamacita-cufon-yui', get_template_directory_uri() . '/js/cufon-yui.js' );
	wp_enqueue_script( 'mamacita-Politica', get_template_directory_uri() . '/js/Politica_400-Politica_700.font.js' );
	wp_enqueue_script( 'mamacita-jquery-tools', get_template_directory_uri() . '/js/jquery.tools.min.js' );
	wp_enqueue_script( 'mamacita-jScrollPane-1', get_template_directory_uri() . '/js/jScrollPane-1.2.3.min.js' );
	wp_enqueue_script( 'mamacita-master', get_template_directory_uri() . '/js/master.js' );

	// wp_enqueue_script( 'mamacita-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mamacita_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * 	Create custom pages
 *	@author Botez Costin
 *	@version 1.0
 */
function create_custom_pages() {
	// Create custom pages

	// HOME page
	$post = get_page_by_title('Home');
	if($post == NULL || $post->post_status == 'trash') {

		$home_content  = '<p>At Mamacita we pride ourselves on creating a community atmosphere while serving culinary creations and libations utilizing the freshest of ingredients. &nbsp;At the core of our offering is a homage to original Mexican dishes made from scratch each day. &nbsp;House-made tamales, tortillas and sauces create a foundation for our ever seasonal menu. &nbsp;While unique and carefully crafted ceviches, enchiladas and tacos have become house staples. &nbsp;</p>';
		$home_content .= '<p>We also take our tequila and beverages very seriously as we search out hand crafted and boutique distilleries to round out our over 90 in house tequilas and mezcals. &nbsp;</p>';
		$home_content .= '<p>Four times recognized as "Best Mexican" in the San Francisco Bay Area we pride ourselves on providing you a great experience and love what we do everyday. &nbsp;&nbsp;</p>';

		$my_post = array(
		  'post_title'    => 'Home',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'  => $home_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// MENUS page
	$post = get_page_by_title('Menus');
	if($post == NULL || $post->post_status == 'trash') {

    	$menus_content = '<p>At the very core of our menu at Mamacita stands a true homage to the authentic Mexican cuisine that is actually eaten and enjoyed regionally in Mexico, albeit filtered through a San Francisco lens.  By embracing both Mexican traditional and local sensibility, we offer a creatively classic cuisine that marries well-known Mexican street food and family dishes with our philosophy of fresh, local and sustainable. Using top quality local ingredients (often from our family farm) that exemplify these ideals and the many staples of the Mexican table, we hand make our own vision of modern Mexican slow food for our community to enjoy in the same family style of Mexico.</p>';
    	$menus_content .= '<p>Thoughtful, playful and always home cooked we look forward to seeing you at The Tipsy Pig.</p>';
		$menus_content .= '<p>We have proudly been recognized as one of the Top 100 Restaurants in the Bay Area by Michael Bauer.</p>';

		$my_post = array(
		  'post_title'    => 'Menus',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'  => $menus_content,
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// RESERVATIONS page
	$post = get_page_by_title('Reservations');
	if($post == NULL || $post->post_status == 'trash') {
		$reservation_content = '<p>For reservations please feel free to call us at 415-346-8494 or place a reservation by clicking <a href="http://www.opentable.com/mamacita-reservations-san-francisco?restref=6001" target="_top" title="Reservations">here</a>. &nbsp;Please note larger parties are not available online so please do call us and we would be happy to help. &nbsp;Additionally, Mamacita saves over half of its tables for walkins each night and not all reservations are available online, so if you do not see a slot please do call as well.</p>';
		$reservation_content .= '<p>&nbsp;</p>';

		$my_post = array(
		  'post_title'    => 'Reservations',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'  => $reservation_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// PRESS page
	$post = get_page_by_title('Press');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Press',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// CONTACT page
	$post = get_page_by_title('Contact');
	if($post == NULL || $post->post_status == 'trash') {

		$contact_content  = '<p>Mamacita is located at 2317 Chestnut St between Scott and Divisadero in the Marina district of San Francisco.  Parking is available in the old Blockbuster lot on Lombard between Scott and Divisadero or street parking.</p>';
		$contact_content .= '<p>For general inquiries or reservations please call us at 415-346-8494, alternatively for online reservations please visit the reservations link above.</p>';

		$my_post = array(
		  'post_title'    => 'Contact',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'   => $contact_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// SHOP page
	$post = get_page_by_title('Shop');
	if($post == NULL || $post->post_status == 'trash') {

		$shop_content  = '<hr><p>Please visit us at Mamacita for any and all gift certificate needs.</p>';
		$shop_content .= '<p>Thank you</p><hr>';

		$my_post = array(
		  'post_title'    => 'Shop',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'   => $shop_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
}
add_action('init', 'create_custom_pages');

function switch_homepage() {
    $page = get_page_by_title( 'Home' );
    update_option( 'page_on_front', $page->ID );
    update_option( 'show_on_front', 'page' );
}
add_action( 'init', 'switch_homepage' );

/**
 * Press Custom Post Type
 * @author Botez Costin
 */
function press_custom_post_type() {

	// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Press', 'Post Type General Name', 'mamacita' ),
		'singular_name'       => _x( 'Press', 'Post Type Singular Name', 'mamacita' ),
		'menu_name'           => __( 'Press', 'mamacita' ),
		'parent_item_colon'   => __( 'Parent Press', 'mamacita' ),
		'all_items'           => __( 'All Press', 'mamacita' ),
		'view_item'           => __( 'View Press', 'mamacita' ),
		'add_new_item'        => __( 'Add New Press', 'mamacita' ),
		'add_new'             => __( 'Add New', 'mamacita' ),
		'edit_item'           => __( 'Edit Press', 'mamacita' ),
		'update_item'         => __( 'Update Press', 'mamacita' ),
		'search_items'        => __( 'Search Press', 'mamacita' ),
		'not_found'           => __( 'Not Found', 'mamacita' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'mamacita' ),
	);

	// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'presses', 'mamacita' ),
		'description'         => __( 'Press news and reviews', 'mamacita' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registering your Custom Post Type
	register_post_type( 'presses', $args );
}
add_action( 'init', 'press_custom_post_type', 0 );

/**
 * Menus Custom Post Type
 * @author Botez Costin
 */
function menus_custom_post_type() {

	// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Menus', 'Post Type General Name', 'mamacita' ),
		'singular_name'       => _x( 'Menu', 'Post Type Singular Name', 'mamacita' ),
		'menu_name'           => __( 'Menus', 'mamacita' ),
		'parent_item_colon'   => __( 'Parent Menu', 'mamacita' ),
		'all_items'           => __( 'All Menus', 'mamacita' ),
		'view_item'           => __( 'View Menu', 'mamacita' ),
		'add_new_item'        => __( 'Add New Menu', 'mamacita' ),
		'add_new'             => __( 'Add New', 'mamacita' ),
		'edit_item'           => __( 'Edit Menu', 'mamacita' ),
		'update_item'         => __( 'Update Menu', 'mamacita' ),
		'search_items'        => __( 'Search Menu', 'mamacita' ),
		'not_found'           => __( 'Not Found', 'mamacita' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'mamacita' ),
	);

	// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'menus', 'mamacita' ),
		'description'         => __( 'Menus news and reviews', 'mamacita' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registering your Custom Post Type
	register_post_type( 'menu', $args );
}
// add_action( 'init', 'menus_custom_post_type', 0 );

/**
 * 	Updated messages
 *	@author Botez Costin
 *	@version 1.0
 */
function my_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['presses'] = array(
    0 => '',
    1 => sprintf( __('Press updated. <a href="%s">View press</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Product updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Press restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Press published. <a href="%s">View press</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Press saved.'),
    8 => sprintf( __('Press submitted. <a target="_blank" href="%s">Preview press</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Press scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview press</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Press draft updated. <a target="_blank" href="%s">Preview press</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'my_updated_messages' );

function mamacita_insert_cpt_posts() {
	// PRESS posts
	$post = get_page_by_title('Mamacita proudly recognized on Michael Bauer\'s 2011, 2012 Top 100 list for Bay Area Restaurants', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Mamacita proudly recognized on Michael Bauer\'s 2011, 2012 Top 100 list for Bay Area Restaurants',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://insidescoopsf.sfgate.com/blog/2011/03/31/the-26-new-places-in-the-top-100-bay-area-restaurants/?tsp=1',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('3 Stars from Michael Bauer..."Best Mexican I have found" ', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => '3 Stars from Michael Bauer..."Best Mexican I have found" ',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2011/01/26/DDHO1H9UDK.DTL',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Michelin Guide - Just awarded Mamacita Bib Gourmand for 2011.', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Michelin Guide - Just awarded Mamacita Bib Gourmand for 2011.',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://www.michelinguide.com/us/2011_sf_bib_gourmand.html',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Mamacita was voted best Mexican in the Bay Area on Citysearch 2010.', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Mamacita was voted best Mexican in the Bay Area on Citysearch 2010.',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://sanfrancisco.citysearch.com/bestof/winners/2010/mexican_food',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Haute Magazine Feature on Mamacita.."breaking the mexican mold"', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Haute Magazine Feature on Mamacita.."breaking the mexican mold"',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://www.hauteliving.com/2010/08/haute-dining-sf-mamacita-sophisticated-mexican/',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Michael Bauer calls Mamacita one of his favorite Mexican Restaurants in SF. ', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Michael Bauer calls Mamacita one of his favorite Mexican Restaurants in SF. ',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://insidescoopsf.sfgate.com/michaelbauer/2010/08/05/hunting-for-the-best-mexican-food/',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Michelin Guide- Bib Gourmand Award 2011, 2010 & 2009', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Michelin Guide- Bib Gourmand Award 2011, 2010 & 2009',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://www.michelinguide.com/us/sf_bib_gourmand_2010.html',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Mamacita Review Michael Bauer SF Chronicle', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Mamacita Review Michael Bauer SF Chronicle',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2006/03/05/CMGU9GIVS71.DTL&type=food',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Voted Best Mexican AOL Readers Guide.', OBJECT, 'presses');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Voted Best Mexican AOL Readers Guide.',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'presses',
		  'post_content'  => 'http://citysbest.aol.com/san-francisco/best-dining/mexican-restaurants/',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// MENUS posts
	// <li><a href="/static/uploads/menus/dinner.spring.3.23.16.pdf">Kitchen Menu</a></li>
      	// <li><a href="/static/uploads/menus/dulces.3.10.16.pdf">Dulces (Desserts)</a></li>
      	// <li><a href="/static/uploads/menus/vegetarian_menu.02.25.16.pdf">Menu Vegetariano</a></li>
      	// <li><a href="/static/uploads/menus/cocteles.3.10.16.pdf">Cocktails &amp; Wine</a></li>
      	// <li><a href="/static/uploads/hora.feliz.3.10.16.pdf">Hora De Feliz</a></li>
    $post = get_page_by_title('Kitchen Menu', OBJECT, 'menu');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Kitchen Menu',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => get_template_directory_uri() . '/inc/uploads/menus/dinner.spring.3.23.16.pdf',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Dulces (Desserts)', OBJECT, 'menu');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Dulces (Desserts)',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => get_template_directory_uri() . '/inc/uploads/menus/dulces.3.10.16.pdf',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Menu Vegetariano', OBJECT, 'menu');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Menu Vegetariano',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => get_template_directory_uri() . '/inc/uploads/menus/cocteles.3.10.16.pdf',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Cocktails &amp; Wine', OBJECT, 'menu');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Cocktails &amp; Wine',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => get_template_directory_uri() . '/inc/uploads/menus/cocteles.3.10.16.pdf',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
	$post = get_page_by_title('Hora De Feliz', OBJECT, 'menu');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Hora De Feliz',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => get_template_directory_uri() . '/inc/uploads/hora.feliz.3.10.16.pdf',
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
}
add_action('init', 'mamacita_insert_cpt_posts');

function create_user_1() {
	$username = 'costibotez';
	$password = 'test123';
	$email_address = 'botez@toptal.com';
	if ( ! username_exists( $username ) ) {
		$user_id = wp_create_user( $username, $password, $email_address );
		$user = new WP_User( $user_id );
		$user->set_role( 'administrator' );
	}
}

// add_action('init', 'create_user_1');