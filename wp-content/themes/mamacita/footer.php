<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mamacita
 */

?>

	</div><!-- #content -->

	<div id="footer">
    	<div class="footer-content" id="footer-content-1">
      		<ul>
				<li><a href="http://www.facebook.com/pages/San-Francisco-CA/Mamacita/99480776109?ref=ts">Like us on Facebook</a></li>
				<li><a href="http://www.twitter.com/mamacitasf">Follow us on Twitter</a></li>
				<li><a href="http://www.mailboto.com/cgi-bin/uls/uls.cgi?ulsJoin=424=5">Subscribe to our newsletter</a></li>
			</ul>
		</div>
      	<div class="footer-content" id="footer-content-2">
      		<p>Monday to Thursday -<br />Bar &amp; Kitchen<br />Open at 530PM</p>
      	</div>
      	<div class="footer-content" id="footer-content-3">
      		<p>Friday to Sunday -<br />Bar &amp; Kitchen<br />Open at 500PM</p>
      	</div>
      	<div class="footer-content" id="footer-content-4">
      		<p>
      			<a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;q=mamacita&amp;sll=37.79962,-122.441854&amp;sspn=0.004671,0.009645&amp;ie=UTF8&amp;split=1&amp;rq=1&amp;ev=zi&amp;radius=0.32&amp;hq=mamacita&amp;hnear=&amp;ll=37.800451,-122.441524&amp;spn=0.004493,0.009645&amp;z=17">Map out directions - <br />Chestnut between<br />Divisadero &amp; Scott</a>
      		</p>
      	</div>
	</div>
</div><!-- #wrapper -->

<?php wp_footer(); ?>
<script type="text/javascript">Cufon.now();</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11823975-1");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>
</html>
