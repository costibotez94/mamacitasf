<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mamacita
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<meta http-equiv="imagetoolbar" content="false" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<script type="text/javascript">
    Cufon.replace("#address, .footer-content", {trim: "advanced"});
  </script>
</head>

<body <?php body_class(); ?>>
	<div id="wrapper">
    	<div id="header">
    		<div id="logo-message-board-wrapper" class="clearfix">
        		<h1>
        			<a href="<?php echo home_url(); ?>">
        				<?php _e('Mamacita', 'mamacita'); ?>
        			</a>
        		</h1>
        		<div id="message-board">
          			<h3><?php _e('Mamacita Message Board', 'mamacita'); ?></h3>
          			<p><strong><a href="http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2011/01/26/DDHO1H9UDK.DTL">Happily recognized on the 2013, 2012 and 2011 Top 100 &amp; recieved 3 Stars from Michael Bauer who says..."Mamacita serves among the best Mexican food I have found."</a></strong></p>
        		</div>
     		</div>
     		<div id="address-navigation-wrapper" class="clearfix">
        		<div id="address">
          			<?php _e('2317 Chestnut Street', 'mamacita'); ?>
          			<img src="<?php echo get_template_directory_uri(); ?>/inc/images/little-m-trans.png"/>
          			<?php _e('San Francisco, California 94123', 'mamacita'); ?>
          			<img src="<?php echo get_template_directory_uri(); ?>/inc/images/little-m-trans.png"/>
          			<?php _e('415.346.8494', 'mamacita'); ?>
        		</div>
        		<div id="navigation">
	          		<ul>
	            		<li class="active" id="home"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/inc/images/home-trans.png" alt="Home" /></a></li>
	            		<li id="menu"><a href="<?php echo home_url('menus'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/inc/images/menu-trans.png" alt="Menu" /></a></li>
	            		<li id="reservations"><a href="<?php echo home_url('reservations'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/inc/images/reservations-trans.png" alt="Reservations" /></a></li>
	            		<li id="press"><a href="<?php echo home_url('press'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/inc/images/press-trans.png" alt="Press" /></a></li>
	            		<li id="contact"><a href="<?php echo home_url('contact'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/inc/images/contact-trans.png" alt="Contact" /></a></li>
	            		<li id="shop"><a href="<?php echo home_url('shop'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/inc/images/shop-trans.png" alt="Shop" /></a></li>
	          		</ul>
	          		<?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
        		</div>
      		</div>
      	</div>
      	<div id="content" class="clearfix">
