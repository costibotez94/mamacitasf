<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mamacita
 */

get_header(); ?>

	<div id="content-text" class="None">

		<?php
			$args = array(
				'post_type' 		=> 'presses',
				'posts_per_page' 	=> -1,
				'post_status'		=> 'publish',
			);

			$press_query = new WP_Query($args);
			if($press_query->have_posts()) : ?>
			<ul>
				<?php while($press_query->have_posts()) : $press_query->the_post(); ?>
					<li><a target="_blank" href="<?php echo get_the_content(); ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
	</div>
<?php
get_footer();
