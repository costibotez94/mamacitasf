<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mamacita
 */

get_header(); ?>

	<div id="content-text" class="menu-images">
		<?php
			// $args = array(
			// 	'post_type'			=> 'menu',
			// 	'posts_per_page'	=> -1,
			// 	'post_status'		=> 'publish',
			// );
			// $menu_query = new WP_Query($args);

			// if($menu_query->have_posts()) : ?>
			<!-- <ul id="menus">
			<?php //while($menu_query->have_posts()) : $menu_query->the_post(); ?>
				<li><a href="<?php //echo esc_url(get_the_content()); ?>"><?php //the_title(); ?></a></li>
			<?php //endwhile; ?>
    		</ul> -->
			<?php // endif; ?>
			<?php //wp_reset_query(); ?>
		<?php global $post; echo $post->post_content; ?>
	</div>
	<div id="tabs-container" class="clearfix">
  		<div class="images-frame clearfix"></div>
  		<div class="images">
    		<div>
      			<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9594_medium.jpg" />
    		</div>
    		<div>
      			<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9454_medium.jpg" />
    		</div>
    		<div>
      			<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9348_medium.jpg" />
    		</div>
    		<div>
      			<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9280_medium.jpg" />
    		</div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9314_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9496_medium.jpg" />
		    </div>
		    <div>
		      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food_shots/mamacitas_20100812_9428_medium.jpg" />
		    </div>
  		</div>
  		<div class="tabs">
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		    <a href="#"></a>
		</div>
  		<hr class="space"/>
		<div id="backward-forward" class="clearfix">
	    	<a class="backward">&laquo;</a>
		    <a class="forward">&raquo;</a>
	  	</div>
	</div>
<?php
get_footer();
